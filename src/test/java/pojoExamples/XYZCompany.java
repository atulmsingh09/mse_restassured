package pojoExamples;

public class XYZCompany {

    public static void main(String[] args) {
        Employee employeeOne = new Employee();
        employeeOne.setId(1);
        employeeOne.setName("XYZ ABC");
        employeeOne.setMobNo("1234567890");
        employeeOne.setGender("male");
        employeeOne.setDeptId(101);
        employeeOne.setAddress("ADI");
        System.out.println("ID: "+employeeOne.getId()+", Name: "+employeeOne.getName()+
                ", Mobile No "+employeeOne.getMobNo()+", Gender: "+employeeOne.getGender()+
                ", Department ID: "+employeeOne.getDeptId()+
                ", Address: "+employeeOne.getAddress());

        Employee employeeTwo = new Employee();
        employeeTwo.setId(2);
        employeeTwo.setName("ABC MNO");
        employeeTwo.setMobNo("9876543210");
        employeeTwo.setGender("female");
        employeeTwo.setDeptId(102);
        employeeTwo.setAddress("VNS");

        System.out.println("ID: "+employeeTwo.getId()+", Name: "+employeeTwo.getName()+
                ", Mobile No "+employeeTwo.getMobNo()+", Gender: "+employeeTwo.getGender()+
                ", Department ID: "+employeeTwo.getDeptId()+
                ", Address: "+employeeTwo.getAddress());
    }
}
