package pojoExamples;

import io.restassured.RestAssured;

public class DummyAPIForStudent {
    public static void main(String[] args) {
        Student studentOne = new Student();
        studentOne.setEnrollment_no(111);
        studentOne.setFirst_name("Ruby");
        studentOne.setLast_name("Marchbank");
        studentOne.setEmail("rmarchbank0@seattletimes.com");
        studentOne.setGender("Female");
        studentOne.setPercentage_marks(79.69);

        RestAssured
                .given()
                    .log()
                    .all()
                    .body(studentOne)
                .when()
                    .get();
    }
}
