package pojoExamples.convertResponseToPOJO;

import io.restassured.RestAssured;

public class NestedJsonObjectResponseToPOJO {
    public static void main(String[] args) {
        EmployeeWithAddress employeeWithAddress = RestAssured
                .get("https://run.mocky.io/v3/03cfb731-26d7-4a49-861e-48b834dd1064")
                .as(EmployeeWithAddress.class);

        String fn = employeeWithAddress.getFirst_name();
        String ln = employeeWithAddress.getLast_name();
        String prfsn = employeeWithAddress.getProfession();
        int age = employeeWithAddress.getAge();
        double sal = employeeWithAddress.getSalary();

        Address address = employeeWithAddress.getAddress();
        int hNo = address.getHouse_no();
        String strtName = address.getStreet_name();
        String city = address.getCity();
        String state = address.getState();
        String country = address.getCountry();

        System.out.println("First Name: " + fn +
                ", Last Name: " + ln +
                ", Profession: " + prfsn +
                ", Age: " + age +
                ", Salary: " + sal +
                ", House No: " + hNo +
                ", Street Name: " + strtName +
                ", City: " + city +
                ", State: " + state +
                ", Country: " + country);
    }
}
