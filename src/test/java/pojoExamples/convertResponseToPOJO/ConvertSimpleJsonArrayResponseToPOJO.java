package pojoExamples.convertResponseToPOJO;

import io.restassured.RestAssured;

public class ConvertSimpleJsonArrayResponseToPOJO {

    public static void main(String[] args) {
        Address[] addresses = RestAssured
                .get("https://run.mocky.io/v3/8d8b4098-4b6a-4b0d-9e9b-0f8bf75f7181")
                .as(Address[].class);
        System.out.println("Address array size: "+addresses.length);
        System.out.println(addresses[0].getCity());
        System.out.println(addresses[1].getCity());
    }
}
