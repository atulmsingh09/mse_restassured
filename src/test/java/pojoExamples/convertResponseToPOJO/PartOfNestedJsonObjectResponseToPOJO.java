package pojoExamples.convertResponseToPOJO;

import io.restassured.RestAssured;

public class PartOfNestedJsonObjectResponseToPOJO {
    public static void main(String[] args) {
        Address address = RestAssured
                .get("https://run.mocky.io/v3/03cfb731-26d7-4a49-861e-48b834dd1064")
                .jsonPath()
                .getObject("address",Address.class);
        System.out.println(address.getCity());
        System.out.println(address.getState());
    }
}
