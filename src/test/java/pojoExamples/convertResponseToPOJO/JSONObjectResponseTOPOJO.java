package pojoExamples.convertResponseToPOJO;

import io.restassured.RestAssured;

public class JSONObjectResponseTOPOJO {
    public static void main(String[] args) {
        Employee employee = RestAssured
                .get("https://run.mocky.io/v3/98ac9bdd-f659-4d0d-ae74-1571cad75b4a")
                .as(Employee.class);
        String employeeFName = employee.getFirst_name();
        String employeeLName = employee.getLast_name();
        String employeeProfession = employee.getProfession();
        int employeeAge = employee.getAge();
        double employeeSalary = employee.getSalary();
        System.out.println("First Name: "+employeeFName+
                ", Last Name: "+employeeLName+
                ", Profession: "+employeeProfession+
                ", Age: "+employeeAge+
                ", Salary: "+employeeSalary);
    }
}
