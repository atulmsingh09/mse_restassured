package pojoExamples.convertResponseToPOJO;

import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import java.util.List;

public class ConvertSimpleJsonArrayResponseToPOJOUsingList {
    public static void main(String[] args) {
        List<Address> addresses = RestAssured
                .get("https://run.mocky.io/v3/8d8b4098-4b6a-4b0d-9e9b-0f8bf75f7181")
                .as(new TypeRef<List<Address>>() {
                });
        System.out.println("Size " + addresses.size());
        System.out.println(addresses.get(0).getCity());
        System.out.println(addresses.get(1).getCity());
    }
}
