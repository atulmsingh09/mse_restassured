package pojoExamples.nestedJsonObjects;

import java.util.List;

public class StudentClassTwo {
    private int enrollment_no;
    private String first_name;
    private String last_name;
    private String email;
    private String gender;
    private double percentage_marks;
    private List<Address> address;

    public int getEnrollment_no() {
        return enrollment_no;
    }

    public void setEnrollment_no(int enrollment_no) {
        this.enrollment_no = enrollment_no;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public double getPercentage_marks() {
        return percentage_marks;
    }

    public List<Address> getAddress() {
        return address;
    }

    public void setAddress(List<Address> address) {
        this.address = address;
    }

    public void setPercentage_marks(double percentage_marks) {
        this.percentage_marks = percentage_marks;
    }

}
