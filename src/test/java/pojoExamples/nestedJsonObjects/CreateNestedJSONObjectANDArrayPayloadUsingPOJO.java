package pojoExamples.nestedJsonObjects;

import io.restassured.RestAssured;

import java.util.ArrayList;
import java.util.List;

public class CreateNestedJSONObjectANDArrayPayloadUsingPOJO {

    public static void main(String[] args) {
        StudentClassTwo studentOne = new StudentClassTwo();
        studentOne.setEnrollment_no(111);
        studentOne.setFirst_name("Ruby");
        studentOne.setLast_name("Marchbank");
        studentOne.setEmail("rmarchbank0@seattletimes.com");
        studentOne.setGender("Female");
        studentOne.setPercentage_marks(79.69);

        Address addressOne = new Address();
        addressOne.setHouse_no(10);
        addressOne.setStreet_name("Not found address");
        addressOne.setCity("ADI");
        addressOne.setState("GUJ");
        addressOne.setCountry("IN");

        Address addressTwo = new Address();
        addressTwo.setHouse_no(71);
        addressTwo.setStreet_name("No Content");
        addressTwo.setCity("Bengaluru");
        addressTwo.setState("KA");
        addressTwo.setCountry("IN");

        List<Address> allAddresses = new ArrayList<>();
        allAddresses.add(addressOne);
        allAddresses.add(addressTwo);

        studentOne.setAddress(allAddresses);

        RestAssured
                .given()
                .log()
                .all()
                .body(studentOne)
                .when()
                .get();
    }
}
