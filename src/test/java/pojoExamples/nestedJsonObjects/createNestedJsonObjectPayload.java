package pojoExamples.nestedJsonObjects;

import io.restassured.RestAssured;

public class createNestedJsonObjectPayload {

    public static void main(String[] args) {
        StudentClass studentOne = new StudentClass();
        Address addressOne = new Address();

        studentOne.setEnrollment_no(111);
        studentOne.setFirst_name("Ruby");
        studentOne.setLast_name("Marchbank");
        studentOne.setEmail("rmarchbank0@seattletimes.com");
        studentOne.setGender("Female");
        studentOne.setPercentage_marks(79.69);
        addressOne.setHouse_no(10);
        addressOne.setStreet_name("Not found address");
        addressOne.setCity("ADI");
        addressOne.setState("GUJ");
        addressOne.setCountry("IN");
        studentOne.setAddress(addressOne);

        RestAssured
                .given()
                .log()
                .all()
                .body(studentOne)
                .when()
                .get();
    }
}
