package pojoExamples;

import io.restassured.RestAssured;
import java.util.ArrayList;
import java.util.List;

public class DummyAPIForStudentJsonArray {
    public static void main(String[] args) {
        Student studentOne = new Student();
        studentOne.setEnrollment_no(111);
        studentOne.setFirst_name("Ruby");
        studentOne.setLast_name("Marchbank");
        studentOne.setEmail("rmarchbank0@seattletimes.com");
        studentOne.setGender("Female");
        studentOne.setPercentage_marks(79.69);

        Student studentTwo = new Student();
        studentTwo.setEnrollment_no(112);
        studentTwo.setFirst_name("Olive");
        studentTwo.setLast_name("Yew");
        studentTwo.setEmail("olive.yew@seattletimes.com");
        studentTwo.setGender("Male");
        studentTwo.setPercentage_marks(56.12);

        List<Student> jsonArrayStudent = new ArrayList<>();
        jsonArrayStudent.add(studentOne);
        jsonArrayStudent.add(studentTwo);

        RestAssured
                .given()
                .log()
                .all()
                .body(jsonArrayStudent)
                .when()
                .get();
    }
}
