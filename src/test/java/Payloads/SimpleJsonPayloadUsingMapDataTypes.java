package Payloads;

import io.restassured.RestAssured;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class SimpleJsonPayloadUsingMapDataTypes {

    public static void main(String[] args) {
        Map<String, Object> jsonObjectPayload = new LinkedHashMap<>();
        jsonObjectPayload.put("id", 1);
        jsonObjectPayload.put("firstName", "Test");
        jsonObjectPayload.put("lastName", "XYZ");
        jsonObjectPayload.put("married", false);
        jsonObjectPayload.put("salary", 123.45);

        RestAssured
                .given()
                    .log()
                    .all()
                    .body(jsonObjectPayload)
                .when()
                    .get();
    }
}
