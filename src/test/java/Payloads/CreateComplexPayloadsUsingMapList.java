package Payloads;

import io.restassured.RestAssured;

import java.util.*;

public class CreateComplexPayloadsUsingMapList {
    public static void main(String[] args) {
        List<Map<String, Object>> mainJsonArrayPayload = new ArrayList<>();

        Map<String, Object> firstPayload = new LinkedHashMap<>();
        firstPayload.put("id",1);
        firstPayload.put("first_name", "Ruby");
        firstPayload.put("last_name","Marchbank");
        firstPayload.put("email","rmarchbank0@seattletimes.com");
        firstPayload.put("gender","Female");

        /*List<String> mobileNos = new ArrayList<>();
        mobileNos.add("1234567890");
        mobileNos.add("987654321");*/

        List<String> mobileNos = Arrays.asList("1234567890", "987654321");
        firstPayload.put("mobile",mobileNos);

        Map<String, Object> testingSkillPayload = new LinkedHashMap<>();
        testingSkillPayload.put("name", "Testing");
        testingSkillPayload.put("proficiency", "medium");

        firstPayload.put("skills",testingSkillPayload);

        //mainJsonArrayPayload.add(firstPayload);

        Map<String, Object> secondPayload = new LinkedHashMap<>();
        secondPayload.put("id",2);
        secondPayload.put("first_name", "Renelle");
        secondPayload.put("last_name","Dumphreys");
        secondPayload.put("email","rdumphreys1@mashable.com");
        secondPayload.put("gender","Female");

        List<Map<String, Object>> skillsList = new ArrayList<>();

        Map<String, Object> javaSkillPayload = new LinkedHashMap<>();
        javaSkillPayload.put("name", "Java");
        javaSkillPayload.put("proficiency", "medium");

        List<String> javaCertificationList = Arrays.asList("OCJP 11", "OCJP 12");
        javaSkillPayload.put("certifications", javaCertificationList);

        skillsList.add(testingSkillPayload);
        skillsList.add(javaSkillPayload);

        secondPayload.put("skills", skillsList);

        mainJsonArrayPayload.add(firstPayload);
        mainJsonArrayPayload.add(secondPayload);

        RestAssured.given().log().all().body(mainJsonArrayPayload).get();
    }
}
