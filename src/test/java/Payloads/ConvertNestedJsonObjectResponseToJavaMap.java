package Payloads;

import io.restassured.RestAssured;

import java.util.Map;

public class ConvertNestedJsonObjectResponseToJavaMap {
    public static void main(String[] args) {

        Map jsonResponseAsMap = RestAssured
                .get("https://run.mocky.io/v3/a8654884-98e6-4343-9150-cf2f70f038b5")
                .as(Map.class);
        String firstName = (String) jsonResponseAsMap.get("first_name");
        System.out.println(firstName);

        Map<String, Object> skillsMap = (Map<String, Object>) jsonResponseAsMap.get("skills");
        System.out.println(skillsMap.get("name"));
        System.out.println(skillsMap.get("proficiency"));

    }
}
