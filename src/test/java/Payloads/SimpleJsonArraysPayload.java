package Payloads;

import io.restassured.RestAssured;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class SimpleJsonArraysPayload {
    public static void main(String[] args) {
        Map<String, Object> employeeOneJsonPayload = new LinkedHashMap<>();
        employeeOneJsonPayload.put("id", 1);
        employeeOneJsonPayload.put("first_name", "Ruby");
        employeeOneJsonPayload.put("last_name", "Marchbank");
        employeeOneJsonPayload.put("email", "rmarchbank0@seattletimes.com");
        employeeOneJsonPayload.put("gender", "Female");

        Map<String, Object> employeeTwoJsonPayload = new LinkedHashMap<>();
        employeeTwoJsonPayload.put("id", 2);
        employeeTwoJsonPayload.put("first_name", "Renelle");
        employeeTwoJsonPayload.put("last_name", "Dumphreys");
        employeeTwoJsonPayload.put("email", "rdumphreys1@mashable.com");
        employeeTwoJsonPayload.put("gender", "Female");

        List<Map<String, Object>> employeeList = new ArrayList<>();
        employeeList.add(employeeOneJsonPayload);
        employeeList.add(employeeTwoJsonPayload);

        RestAssured
                .given()
                    .log()
                    .all()
                    .body(employeeList)
                .when()
                    .get();
    }
}
