package Payloads;

import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import java.util.Map;

public class ConvertJsonObjectResponseToJavaMapWithGenerics {
    public static void main(String[] args) {

        Map<String, Object> jsonResponseAsMap = RestAssured
                .get("https://run.mocky.io/v3/6495fab9-648b-4891-a99c-057d2d1e9496")
                .as(new TypeRef<Map<String, Object>>(){});
        String firstName = (String) jsonResponseAsMap.get("first_name");
        String lastName = (String) jsonResponseAsMap.get("last_name");
        int id = (int) jsonResponseAsMap.get("id");
        String emailId = (String) jsonResponseAsMap.get("email");
        String gender = (String) jsonResponseAsMap.get("gender");
        System.out.println(firstName);
        System.out.println(lastName);
        System.out.println(id);
        System.out.println(emailId);
        System.out.println(gender);

        jsonResponseAsMap.keySet().forEach(k -> System.out.println(k));
        jsonResponseAsMap.values().forEach(k -> System.out.println(k));
    }
}
