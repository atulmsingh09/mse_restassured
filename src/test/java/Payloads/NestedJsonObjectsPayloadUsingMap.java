package Payloads;

import io.restassured.RestAssured;

import java.util.LinkedHashMap;
import java.util.Map;

public class NestedJsonObjectsPayloadUsingMap {

    public static void main(String[] args) {
        Map<String, Object> nestedJsonObjectPayload = new LinkedHashMap<>();
        nestedJsonObjectPayload.put("id", 1);
        nestedJsonObjectPayload.put("firstName", "Test");
        nestedJsonObjectPayload.put("lastName", "XYZ");
        nestedJsonObjectPayload.put("married", false);
        nestedJsonObjectPayload.put("salary", 123.45);

        Map<String, Object> addressMap = new LinkedHashMap<>();
        addressMap.put("no", 1);
        addressMap.put("street", "XYZ street");
        addressMap.put("city", "ADI");
        addressMap.put("state", "GU");

        nestedJsonObjectPayload.put("address",addressMap );

        RestAssured
                .given()
                    .log()
                    .all()
                    .body(nestedJsonObjectPayload)
                .when()
                    .get();
    }
}
