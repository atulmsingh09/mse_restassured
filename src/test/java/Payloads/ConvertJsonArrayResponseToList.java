package Payloads;

import io.restassured.RestAssured;
import io.restassured.common.mapper.TypeRef;
import java.util.List;
import java.util.Map;

public class ConvertJsonArrayResponseToList {
    public static void main(String[] args) {
       /* List<Object> allEmp = RestAssured
                .get("https://run.mocky.io/v3/9acc2ed1-6ded-4052-8364-3321b974386a")
                .as(List.class);*/
        List<Map<String, Object>> allEmp = RestAssured
                .get("https://run.mocky.io/v3/9acc2ed1-6ded-4052-8364-3321b974386a")
                .as(new TypeRef<List<Map<String, Object>>>() {
                });

       /* Map<String,Object> empOne = (Map<String, Object>) allEmp.get(0);
        System.out.println(empOne.get("first_name"));*/
        //System.out.println("Size: "+allEmp.size());

        Map<String, Object> empOne = allEmp.get(0);
        System.out.println(empOne.get("first_name"));
    }
}
