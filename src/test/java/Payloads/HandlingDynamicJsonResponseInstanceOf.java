package Payloads;

import io.restassured.RestAssured;
import io.restassured.response.Response;

import java.util.List;
import java.util.Map;

public class HandlingDynamicJsonResponseInstanceOf {
    public static void main(String[] args) {
        Response response = RestAssured
                //.get("https://run.mocky.io/v3/5fef2614-be12-4197-ae87-02830f73ba1c");
                .get("https://run.mocky.io/v3/6495fab9-648b-4891-a99c-057d2d1e9496");

        Object responseAsObject = response.as(Object.class);
        if (responseAsObject instanceof List){
            List responseAsList = (List) responseAsObject;
            System.out.println(responseAsList.size());
        }
        else if (responseAsObject instanceof Map){
            Map responseAsMap = (Map) responseAsObject;
            System.out.println(responseAsMap.keySet());
        }
    }
}
