package restFulBooker;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.hamcrest.Matchers;

import java.util.concurrent.TimeUnit;

public class MeasureresponseTime {

    public static void main(String[] args) {
        Response response = RestAssured
                .given()
                .baseUri("https://restful-booker.herokuapp.com/")
                .basePath("booking")
                .body("{\n" +
                        "    \"firstname\" : \"FirstName01\",\n" +
                        "    \"lastname\" : \"LastName01\",\n" +
                        "    \"totalprice\" : 111,\n" +
                        "    \"depositpaid\" : true,\n" +
                        "    \"bookingdates\" : {\n" +
                        "        \"checkin\" : \"2018-01-01\",\n" +
                        "        \"checkout\" : \"2019-01-01\"\n" +
                        "    },\n" +
                        "    \"additionalneeds\" : \"Lunch\"\n" +
                        "}")
                //Hit request and get response
                .contentType(ContentType.JSON)
                .post();

                long responseTimeInMS = response.time();
                System.out.println("Response time in MS: "+responseTimeInMS);

                long responseTimeInSeconds = response.timeIn(TimeUnit.SECONDS);
                System.out.println("Response time in Seconds: "+responseTimeInSeconds);

                long responseTimeInMSOne = response.getTime();
                System.out.println("Response time in MS 1: "+responseTimeInMSOne);

                long responseTimeInSecondsOne = response.getTimeIn(TimeUnit.SECONDS);
                System.out.println("Response time in Seconds 1: "+responseTimeInSecondsOne);

                //response.then().time(Matchers.lessThan(5000L));
                //response.then().time(Matchers.greaterThan(2000L));
                response.then().time(Matchers.both(Matchers.greaterThan(2000L)).and(Matchers.lessThan(5000L)));
                response.then().time(Matchers.lessThan(5L), TimeUnit.SECONDS);

    }
}
