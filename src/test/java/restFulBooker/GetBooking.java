package restFulBooker;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;

public class GetBooking {
    public static void main(String[] args) {
        /*//GIVEN - Build request
        RequestSpecification requestSpecification = RestAssured.given().log().all();
        requestSpecification.baseUri("https://restful-booker.herokuapp.com/");
        requestSpecification.basePath("booking/{id}");
        requestSpecification.pathParam("id",20);

        //WHEN - Hit request and get response
        Response response = requestSpecification.get();

        //THEN - Validate response
        ValidatableResponse validatableResponse = response.then().log().all();
        validatableResponse.statusCode(200);*/


        RestAssured
                .given()            //GIVEN - Build request
                    .log()
                    .all()
                    .baseUri("https://restful-booker.herokuapp.com/")
                    .basePath("{basePath}/{id}")
                    .pathParam("basePath","booking")
                    .pathParam("id",2)
                .when()             //WHEN - Hit request and get response
                    .get()
                .then()             //THEN - Validate response
                    .log()
                    .all()
                    .statusCode(200);
    }
}
