package restFulBooker;

import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HeaderExample {

    @Test
    public void passHeader(){
        RestAssured
                .given()
                .log()
                .all()
                .header("HeaderOne", "valueOne")
                .header("HeaderTwo", "valueOne", "valueTwo","valueThree")
                .when()
                .get();
    }

    @Test
    public void passHeader1(){
        RestAssured
                .given()
                .log()
                .all()
                .header("HeaderOne", "valueOne")
                .header("HeaderOne", "valueTwo")
                .when()
                .get();
    }

    @Test
    public void passHeader2(){
        Header header = new Header("Header","Value");
        RestAssured
                .given()
                .log()
                .all()
                .header(header)
                .when()
                .get();
    }

    @Test
    public void passHeader3(){
        RestAssured
                .given()
                .log()
                .all()
                .headers("H1","V1","H2","V2", "H3","V3")
                .when()
                .get();
    }

    @Test
    public void passHeader4(){
        Map<String, String> headersMap = new HashMap<>();
        headersMap.put("H1","V1");
        headersMap.put("H2","V2");
        headersMap.put("H3","V3");
        headersMap.put("H3","V4");

        RestAssured
                .given()
                .log()
                .all()
                .headers(headersMap)
                .when()
                .get();
    }

    @Test
    public void passHeader5(){
        Header header = new Header("Header","Value");
        Headers headers = new Headers(header);
        RestAssured
                .given()
                .log()
                .all()
                .headers(headers)
                .when()
                .get();
    }

    @Test
    public void passHeader6(){
        List<Header> allHeaders = new ArrayList<>();
        Header header1 = new Header("H1","V1");
        Header header2 = new Header("H2","V2");
        Header header3 = new Header("H3","V3");
        Header header4 = new Header("H3","V4");
        allHeaders.add(header1);
        allHeaders.add(header2);
        allHeaders.add(header3);
        allHeaders.add(header4);
        Headers headers = new Headers(allHeaders);
        RestAssured
                .given()
                .log()
                .all()
                .headers(headers)
                .when()
                .get();
    }
}
