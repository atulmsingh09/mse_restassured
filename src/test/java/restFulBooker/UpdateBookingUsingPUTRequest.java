package restFulBooker;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class UpdateBookingUsingPUTRequest {
    public static void main(String[] args) {
        RestAssured
                .given()
                    .log()
                    .all()
                    .baseUri("https://restful-booker.herokuapp.com/")
                    .basePath("booking/2")
                    .contentType(ContentType.JSON)
                    .header("Authorization","Basic YWRtaW46cGFzc3dvcmQxMjM=")
                    .body("{\n" +
                            "    \"firstname\" : \"ABS\",\n" +
                            "    \"lastname\" : \"XYZ\",\n" +
                            "    \"totalprice\" : 111,\n" +
                            "    \"depositpaid\" : true,\n" +
                            "    \"bookingdates\" : {\n" +
                            "        \"checkin\" : \"2018-01-01\",\n" +
                            "        \"checkout\" : \"2019-01-01\"\n" +
                            "    },\n" +
                            "    \"additionalneeds\" : \"Breakfast\"\n" +
                            "}")
                .when()
                    .put()
                .then()
                    .log()
                    .all()
                    .assertThat()
                    .statusCode(200);
    }
}
