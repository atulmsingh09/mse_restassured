package restFulBooker;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class RestAssuredScriptInBDDFormat {
    public static void main(String[] args) {
        RestAssured
                .given()        //GIVEN - Build request
                    .baseUri("https://restful-booker.herokuapp.com/")
                    .basePath("booking")
                    .body("{\n" +
                            "    \"firstname\" : \"Jim\",\n" +
                            "    \"lastname\" : \"Brown\",\n" +
                            "    \"totalprice\" : 111,\n" +
                            "    \"depositpaid\" : true,\n" +
                            "    \"bookingdates\" : {\n" +
                            "        \"checkin\" : \"2018-01-01\",\n" +
                            "        \"checkout\" : \"2019-01-01\"\n" +
                            "    },\n" +
                            "    \"additionalneeds\" : \"Breakfast\"\n" +
                            "}")
                    .contentType(ContentType.JSON)
                .when()         //WHEN - hit request and get response
                    .post()
                .then()         //THEN - validate response
                    .statusCode(200);
    }
}
