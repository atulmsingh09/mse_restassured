package restFulBooker;

import io.restassured.RestAssured;

public class PathParameterExample {

    public static void main(String[] args) {

        RestAssured
                .given()
                    .log()
                    .all()
                    //.baseUri("https://restful-booker.herokuapp.com/")
                    //.basePath("{basePath}/{id}")
                .when()
                    .get("https://restful-booker.herokuapp.com/{basePath}/{id}","booking",2)
                .then()
                    .log()
                    .all();
    }
}
