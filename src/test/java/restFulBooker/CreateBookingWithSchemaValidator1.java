package restFulBooker;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.module.jsv.JsonSchemaValidator;

import java.io.File;

public class CreateBookingWithSchemaValidator1 {
    public static void main(String[] args) {
        String jsonScheamPath = System.getProperty("user.dir")+"\\src\\test\\java\\restFulBooker\\CreateBookingResponseSchema1.json";
        //Build Request
        RestAssured
                .given()
                .log()
                .all()
                .baseUri("https://restful-booker.herokuapp.com/")
                .basePath("booking")
                .body("{\n" +
                        "    \"firstname\" : \"FirstName01\",\n" +
                        "    \"lastname\" : \"LastName01\",\n" +
                        "    \"totalprice\" : 111,\n" +
                        "    \"depositpaid\" : true,\n" +
                        "    \"bookingdates\" : {\n" +
                        "        \"checkin\" : \"2018-01-01\",\n" +
                        "        \"checkout\" : \"2019-01-01\"\n" +
                        "    },\n" +
                        "    \"additionalneeds\" : \"Lunch\"\n" +
                        "}")
                //Hit request and get response
                .contentType(ContentType.JSON)
                .post()
                .then()
                .log()
                .all()
                //Validate response
                .statusCode(200)
                .body(JsonSchemaValidator.matchesJsonSchema(new File(System.getProperty("user.dir")+"\\src\\test\\java\\restFulBooker\\CreateBookingResponseSchema1.json")));
    }

}
