package restFulBooker;

import io.restassured.RestAssured;
import io.restassured.config.HeaderConfig;
import io.restassured.config.RestAssuredConfig;
import org.testng.annotations.Test;

public class DefaultHeaderBehaviour {

    @Test
    public void defaultBehaviour(){
        RestAssured
                .given()
                .log()
                .all()
                .header("header1","value1")
                .header("header1","value1")
                .when()
                .get();
    }

    @Test
    public void overWriteValue(){
        RestAssured
                .given()
                .log()
                .all()
                .config(RestAssuredConfig.config()
                        .headerConfig(HeaderConfig
                                .headerConfig()
                                .overwriteHeadersWithName("header1","header2")))
                .config(RestAssuredConfig.config()
                        .headerConfig(HeaderConfig.headerConfig().mergeHeadersWithName("header1")))
                .header("header1","value1")
                .header("header1","value2")
                .header("header3","value3")
                .header("header3","value4")
                .header("header2","value5")
                .header("header2","value6")
                .when()
                .get();
    }
}
