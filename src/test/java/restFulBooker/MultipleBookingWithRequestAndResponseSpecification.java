package restFulBooker;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.hamcrest.Matchers;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MultipleBookingWithRequestAndResponseSpecification {
    RequestSpecification requestSpecification;
    ResponseSpecification responseSpecification;

    @BeforeClass
    public void setup() {
        requestSpecification = RestAssured.given();
        requestSpecification.log().all();
        requestSpecification.baseUri("https://restful-booker.herokuapp.com/");
        requestSpecification.contentType(ContentType.JSON);
        requestSpecification.body("{\n" +
                "    \"firstname\" : \"FirstName01\",\n" +
                "    \"lastname\" : \"LastName01\",\n" +
                "    \"totalprice\" : 111,\n" +
                "    \"depositpaid\" : true,\n" +
                "    \"bookingdates\" : {\n" +
                "        \"checkin\" : \"2018-01-01\",\n" +
                "        \"checkout\" : \"2019-01-01\"\n" +
                "    },\n" +
                "    \"additionalneeds\" : \"Lunch\"\n" +
                "}");

        responseSpecification = RestAssured.expect();
        responseSpecification.statusCode(200);
        responseSpecification.contentType(ContentType.JSON);
        responseSpecification.time(Matchers.lessThan(5000L));
    }

    @Test
    public void createBookingOne() {
        RestAssured
                .given(requestSpecification)
                .basePath("booking")
                .post()
                .then()
                .log()
                .all()
                .spec(responseSpecification);
    }

    @Test
    public void updateBooking() {
        RestAssured
                .given(requestSpecification)
                .basePath("booking/2")
                .header("Authorization", "Basic YWRtaW46cGFzc3dvcmQxMjM=")
                .when()
                .put()
                .then()
                .log()
                .all()
                .spec(responseSpecification);
    }
}
