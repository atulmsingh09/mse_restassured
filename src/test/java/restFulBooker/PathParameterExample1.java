package restFulBooker;

import io.restassured.RestAssured;
import java.util.HashMap;
import java.util.Map;

public class PathParameterExample1 {

    public static void main(String[] args) {
        Map<String, Object> pathParameters = new HashMap<>();
        pathParameters.put("basePath", "booking");
        pathParameters.put("id", 1);

        RestAssured
                .given()
                    .log()
                    .all()
                    .baseUri("https://restful-booker.herokuapp.com/")
                    .basePath("{basePath}/{id}")
                    .pathParams(pathParameters)
                .when()
                    .get()
                .then()
                    .log()
                    .all()
                    .assertThat()
                    .statusCode(200);
    }
}
