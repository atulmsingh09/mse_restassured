package restFulBooker;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class MultipleBookingWithRequestSpecification {
    RequestSpecification requestSpecification;

    @BeforeClass
    public void setup() {
        requestSpecification = RestAssured.given();
        requestSpecification
                .log()
                .all()
                .baseUri("https://restful-booker.herokuapp.com/")
                .contentType(ContentType.JSON);
    }

    @Test
    public void createBookingOne() {
        RestAssured
                .given()
                .spec(requestSpecification)
                .basePath("booking")
                .body("{\n" +
                        "    \"firstname\" : \"FirstName01\",\n" +
                        "    \"lastname\" : \"LastName01\",\n" +
                        "    \"totalprice\" : 111,\n" +
                        "    \"depositpaid\" : true,\n" +
                        "    \"bookingdates\" : {\n" +
                        "        \"checkin\" : \"2018-01-01\",\n" +
                        "        \"checkout\" : \"2019-01-01\"\n" +
                        "    },\n" +
                        "    \"additionalneeds\" : \"Lunch\"\n" +
                        "}")
                .post()
                .then()
                .log()
                .all()
                .statusCode(200);
    }

    @Test
    public void updateBooking() {
        RestAssured
                .given()
                .spec(requestSpecification)
                .basePath("booking/2")
                .header("Authorization", "Basic YWRtaW46cGFzc3dvcmQxMjM=")
                .body("{\n" +
                        "    \"firstname\" : \"ABS\",\n" +
                        "    \"lastname\" : \"XYZ\",\n" +
                        "    \"totalprice\" : 111,\n" +
                        "    \"depositpaid\" : true,\n" +
                        "    \"bookingdates\" : {\n" +
                        "        \"checkin\" : \"2018-01-01\",\n" +
                        "        \"checkout\" : \"2019-01-01\"\n" +
                        "    },\n" +
                        "    \"additionalneeds\" : \"Breakfast\"\n" +
                        "}")
                .when()
                .put()
                .then()
                .log()
                .all()
                .assertThat()
                .statusCode(200);
    }
}
