package jsonPathExamples;

import io.restassured.path.json.JsonPath;

import java.util.List;

public class JsonPathForJsonArray {
    public static void main(String[] args) {
        String jsonArray = "[\n" +
                "  [\n" +
                "    \"10\",\n" +
                "    \"20\",\n" +
                "    \"30\",\n" +
                "    \"40\",\n" +
                "    \"50\"\n" +
                "  ],\n" +
                "  [\n" +
                "    \"100\",\n" +
                "    \"200\",\n" +
                "    \"300\",\n" +
                "    \"400\",\n" +
                "    \"500\",\n" +
                "    \"600\"\n" +
                "  ]\n" +
                "]";

        JsonPath jsonPath = new JsonPath(jsonArray);
        System.out.println(jsonPath.getString("[1][0]"));

        System.out.println(jsonPath.getList("$").size());

        List<Object> innerList = (List<Object>) jsonPath.getList("$").get(1);
        System.out.println(innerList.size());

    }
}
