package jsonPathExamples;

import io.restassured.path.json.JsonPath;
import org.testng.annotations.Test;

public class JsonPathIntro {
    @Test
    public void jsonPathDemo() {

        String json = "{\n" +
                "    \"firstname\" : \"Jim\",\n" +
                "    \"lastname\" : \"Brown\",\n" +
                "}";

        JsonPath jsonPath = new JsonPath(json);
        String firsName = jsonPath.getString("firstname");
        System.out.println("First name String data type: " + firsName);

        Object fName = jsonPath.getString("firstname");
        System.out.println("First name using Object: " + fName);
    }
}
