package jsonPathExamples;

import io.restassured.path.json.JsonPath;

import java.io.File;
import java.util.List;

public class JsonPathWithFilters1 {
    public static void main(String[] args) {
        String filePath = System.getProperty("user.dir")+"\\src\\test\\java\\jsonPathExamples\\People1.json";
        File jsonArrayFile = new File(filePath);

        JsonPath jsonPath = new JsonPath(jsonArrayFile);
        System.out.println(jsonPath.getString("data[0].first_name"));

        List<String> listOfManWithFirstName = jsonPath.getList("data.findAll{it.gender == 'Male'}.first_name");
        System.out.println(listOfManWithFirstName);
    }

}
