package jsonPathExamples;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import org.hamcrest.Matchers;

public class InlineAssertionsForArray {

    public static void main(String[] args) {
        RestAssured
                .given()
                .log()
                .all()
                .baseUri("https://restful-booker.herokuapp.com/")
                .basePath("booking")
                .contentType(ContentType.JSON)
                .when()
                .get()
                .then()
                .log()
                .all()
                .body("bookingid", Matchers.hasItems(9, 10));
    }
}
