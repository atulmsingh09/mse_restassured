package jsonPathExamples;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import org.testng.Assert;

public class WithoutInlineAssertion {

    public static void main(String[] args) {
        String jsonResponse = RestAssured
                .given()
                .log()
                .all()
                .baseUri("https://restful-booker.herokuapp.com/")
                .basePath("auth")
                .body("{\n" +
                        "    \"username\" : \"admin\",\n" +
                        "    \"password\" : \"password123\"\n" +
                        "}")
                .contentType(ContentType.JSON)
                .when()
                .post()
                .then()
                .log()
                .all()
                .extract()
                .asString();

        JsonPath jsonPath = new JsonPath(jsonResponse);
        System.out.println("Token: " + jsonPath.get("token"));
        Assert.assertNotNull(jsonPath.get("token"));
    }
}
