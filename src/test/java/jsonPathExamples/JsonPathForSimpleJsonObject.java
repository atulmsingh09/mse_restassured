package jsonPathExamples;

import io.restassured.path.json.JsonPath;
import org.testng.annotations.Test;

public class JsonPathForSimpleJsonObject {

    @Test
    public void jsonPathSimpleObject(){
        String jsonObject = "{\n" +
                "    \"firstname\" : \"Jim\",\n" +
                "    \"lastname\" : \"Brown\",\n" +
                "    \"age\" : 29,\n" +
                "    \"address\" : \"Gujarat\",\n" +
                "    \"salary\" : 10.5,\n" +
                "    \"married\" : false\n" +
                "}";

        JsonPath jsonPath = new JsonPath(jsonObject);
        String firstName = jsonPath.getString("firstname");
        System.out.println(firstName);

        int age = jsonPath.getInt("age");
        System.out.println(age);

        boolean married = jsonPath.getBoolean("married");
        System.out.println(married);

        float salary = jsonPath.getFloat("salary");
        System.out.println(salary);

        double doubleSalary = jsonPath.getDouble("salary");
        System.out.println(doubleSalary);
    }
}
