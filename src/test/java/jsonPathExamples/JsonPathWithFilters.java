package jsonPathExamples;

import io.restassured.path.json.JsonPath;

import java.io.File;
import java.util.List;

public class JsonPathWithFilters {
    public static void main(String[] args) {
        String filePath = System.getProperty("user.dir")+"\\src\\test\\java\\jsonPathExamples\\People.json";
        File jsonArrayFile = new File(filePath);

        JsonPath jsonPath = new JsonPath(jsonArrayFile);
        System.out.println(jsonPath.getString("[0].first_name"));
        List<String> listFirstName = jsonPath.getList("first_name");
        System.out.println(listFirstName);

        List<String> listOfManWithFirstName = jsonPath.getList("findAll{it.gender == 'Male'}.first_name");
        System.out.println(listOfManWithFirstName);

        List<String> listOfWomanWithFirstName = jsonPath.getList("findAll{it.gender == 'Female'}.first_name");
        System.out.println(listOfWomanWithFirstName);

        String emailIDOFLothaire = jsonPath.getString("find{it.first_name == 'Lothaire' & it.last_name == 'Benazet'}.email");
        System.out.println(emailIDOFLothaire);

        List<String> allEmilId = jsonPath.getList("findAll{it.first_name == 'Lothaire' | it.last_name == 'Cowser'}.email");
        System.out.println(allEmilId);

        System.out.println(jsonPath.getList("findAll{it.id >= 5}.first_name"));
        System.out.println(jsonPath.getInt("size()"));
    }

}
