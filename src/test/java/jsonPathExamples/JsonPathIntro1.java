package jsonPathExamples;

import io.restassured.path.json.JsonPath;
import org.testng.annotations.Test;

public class JsonPathIntro1 {
    @Test
    public void jsonPathDemo1() {

        String json = "{\n" +
                "    \"firstname\" : \"Jim\",\n" +
                "    \"lastname\" : \"Brown\",\n" +
                "}";

        JsonPath jsonPath = new JsonPath(json);
        System.out.println((Object)jsonPath.get("$"));
        System.out.println((Object)jsonPath.get());
        System.out.println(jsonPath.getString("$"));
        System.out.println(jsonPath.getString(""));
    }
}
