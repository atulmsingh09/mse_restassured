package jsonPathExamples;

import io.restassured.path.json.JsonPath;

import java.util.List;

public class JsonPathForJsonArrays {
    public static void main(String[] args) {
        String jsonArray = "[\n" +
                "  {\n" +
                "    \"firstname\": \"Jim\",\n" +
                "    \"lastname\": \"Brown\",\n" +
                "    \"age\": 29,\n" +
                "    \"address\": [\n" +
                "      {\n" +
                "        \"city\": \"Mumbai\",\n" +
                "        \"state\": \"Maharastra\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"city\": \"Bengluru\",\n" +
                "        \"state\": \"karnataka\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"city1\": \"Bengluru\",\n" +
                "        \"state1\": \"karnataka\"\n" +
                "      }\n" +
                "    ],\n" +
                "    \"salary\": 10.5,\n" +
                "    \"married\": false\n" +
                "  },\n" +
                "  {\n" +
                "    \"firstname\": \"Test\",\n" +
                "    \"lastname\": \"ABC\",\n" +
                "    \"age\": 29,\n" +
                "    \"address\": [\n" +
                "      {\n" +
                "        \"city\": \"Pune\",\n" +
                "        \"state\": \"Maharastra\"\n" +
                "      },\n" +
                "      {\n" +
                "        \"city\": \"Patna\",\n" +
                "        \"state\": \"Bihar\"\n" +
                "      }\n" +
                "    ],\n" +
                "    \"salary\": 20.5,\n" +
                "    \"married\": true\n" +
                "  }\n" +
                "]";

        JsonPath jsonPath = new JsonPath(jsonArray);
        String cityOne = jsonPath.getString("[0].address[0].city");
        System.out.println(cityOne);

        String cityTwo = jsonPath.getString("[1].address[1].city");
        System.out.println(cityTwo);

        List<String> lsiOfCities = jsonPath.getList("[0].address.city");
        System.out.println(lsiOfCities);

        System.out.println(jsonPath.getList("address.city"));

    }
}
