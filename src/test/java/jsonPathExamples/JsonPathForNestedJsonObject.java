package jsonPathExamples;

import io.restassured.path.json.JsonPath;
import org.testng.annotations.Test;

public class JsonPathForNestedJsonObject {

    @Test
    public void jsonPathSimpleObject() {
        String jsonObject = "{\n" +
                "  \"firstname\": \"Jim\",\n" +
                "  \"lastname\": \"Brown\",\n" +
                "  \"age\": 29,\n" +
                "  \"address\": {\n" +
                "    \"city\": \"Mumbai\",\n" +
                "    \"state\": \"Maharastra\",\n" +
                "    \"pincode\": 123456,\n" +
                "    \"area\": \"Andheri\"\n" +
                "  },\n" +
                "  \"salary\": 10.5,\n" +
                "  \"married\": false\n" +
                "}";

        JsonPath jsonPath = new JsonPath(jsonObject);
        String city = jsonPath.getString("address.city");
        System.out.println(city);

        int pincode = jsonPath.getInt("address.pincode");
        System.out.println(pincode);

        Object object =  jsonPath.get("address");
        System.out.println(object);
    }
}
