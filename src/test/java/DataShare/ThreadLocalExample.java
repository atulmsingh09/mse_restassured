package DataShare;

public class ThreadLocalExample {
    public static void main(String[] args) {
        ThreadLocal counter1 = new ThreadLocal();
        counter1.set("Atul");
        System.out.println(counter1.get());

        counter1.remove();
        System.out.println(counter1.get());

        ThreadLocal counter2 = ThreadLocal.withInitial(() -> "Atul Singh");
        System.out.println(counter2.get());

        ThreadLocal<String> counter3 = new ThreadLocal<>();
        counter3.set("Test");
        System.out.println(counter3.get());


    }
}
