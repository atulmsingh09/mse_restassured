package DataShare;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.testng.annotations.Test;

public class ShareDataAmongMethods {
    private int bookingId;

    @Test(priority = 1)
    public void createBooking() {
         bookingId = RestAssured
                .given()
                .log()
                .all()
                .baseUri("https://restful-booker.herokuapp.com/")
                .basePath("booking")
                .body("{\n" +
                        "    \"firstname\" : \"FirstName01\",\n" +
                        "    \"lastname\" : \"LastName01\",\n" +
                        "    \"totalprice\" : 111,\n" +
                        "    \"depositpaid\" : true,\n" +
                        "    \"bookingdates\" : {\n" +
                        "        \"checkin\" : \"2018-01-01\",\n" +
                        "        \"checkout\" : \"2019-01-01\"\n" +
                        "    },\n" +
                        "    \"additionalneeds\" : \"Lunch\"\n" +
                        "}")
                //Hit request and get response
                .contentType(ContentType.JSON)
                .post()
                .then()
                .log()
                .all()
                //Validate response
                .statusCode(200)
                .extract()
                .jsonPath()
                .getInt("bookingid");
    }

    @Test(priority = 2)
    public void retrieveBooking() {
        Response response = RestAssured
                .given()
                .log()
                .all()
                .get("https://restful-booker.herokuapp.com/booking/" + bookingId)
                .then().log().all().extract().response();
    }
}
