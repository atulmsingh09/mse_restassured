package DataShare;

import java.util.LinkedHashMap;
import java.util.Map;

public class DataStoreAsMap {
    private DataStoreAsMap() {
    }

    private static Map<String, Object> dataMap = new LinkedHashMap<>();

    public static void setValue(String key, Object value) {
        dataMap.put(key, value);
    }

    public static Object getValue(String key) {
        return dataMap.get(key);
    }
}
