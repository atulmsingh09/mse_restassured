package DataShare;

import io.restassured.RestAssured;
import org.testng.ITestContext;
import org.testng.annotations.Test;

public class GetBookingID {
    @Test
    public void getBookingID(ITestContext iTestContext){
        RestAssured
                .given()
                .log()
                .all()
                //.basePath("https://restful-booker.herokuapp.com/booking/"+iTestContext.getAttribute("bookingId"))
                .when()
                .get("https://restful-booker.herokuapp.com/booking/"+iTestContext.getAttribute("bookingId"))
                .then()
                .log()
                .all()
                .extract()
                .response();
    }
}
